
import '../Models/person.dart';

class ConnectDB {

  static List<Person> all() {
    return [
      const Person(
        name: 'Рик Санчез',
        species: 'Человек',
        status: 'Alive',
        gender: 'Мужской',
      ),
      const Person(
        name: 'Алан Райс',
        species: 'Человек',
        status: 'Dead',
        gender: 'Мужской',
      ),
      const Person(
        name: 'Саммер Смит',
        species: 'Человек',
        status: 'Alive',
        gender: 'Женский',
      ),
      const Person(
        name: 'Морти Смит',
        species: 'Человек',
        status: 'Alive',
        gender: 'Мужской',
      ),
    ];
  }

}