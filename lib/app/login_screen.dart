import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lesson5/constants/app_assets.dart';

import '../constants/app_button_styles.dart';
import '../constants/app_colors.dart';
import '../constants/app_styles.dart';
import 'list_persons.dart';
import '../generated/l10n.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State {
  final formKey = GlobalKey<FormState>();

  final int _minCharLogin = 3;
  final int _maxCharLogin = 8;

  final int _minCharPass = 8;
  final int _maxCharPass = 16;

  String _login = "";
  String _pass = "";

  bool _accountCorrect(String login, String password) {
    return login == 'qwerty' && password == '123456ab';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.all(10.0),
          child: Form(
              key: formKey,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Image.asset(AppAssets.images.logo),
                  ),
                  TextFormField(
                    style: AppStyles.s16w400.copyWith(
                      color: AppColors.mainText,
                    ),
                    decoration: InputDecoration(
                      hintText: S.of(context).login,
                      hintStyle: AppStyles.s16w400.copyWith(
                        color: AppColors.neutral2,
                      ),
                      prefixIcon: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                        ),
                        child: SvgPicture.asset(
                          AppAssets.svg.account,
                          width: 16.0,
                          color: AppColors.neutral2,
                        ),
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 16.0,
                      ),
                      filled: true,
                      fillColor: AppColors.neutral1,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        borderSide: BorderSide.none,
                      ),
                      counterText: '',
                    ),
                    maxLength: _maxCharLogin,
                    onTap: () {
                      FocusScope.of(context).unfocus();
                    },
                    onChanged: (value) {
                      setState(() {
                        _login = value;
                      });
                    },
                    validator: (String? value) {
                      return value == null || value.isEmpty
                          ? S.of(context).inputErrorCheckLogin
                          : value.length < _minCharLogin
                              ? S.of(context).inputErrorCheckLogin
                              : null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    style: AppStyles.s16w400.copyWith(
                      color: AppColors.mainText,
                    ),
                    maxLength: _maxCharPass,
                    obscureText: true,
                    obscuringCharacter: '・',
                    decoration: InputDecoration(
                      hintText: S.of(context).password,
                      hintStyle: AppStyles.s16w400.copyWith(
                        color: AppColors.neutral2,
                      ),
                      prefixIcon: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                        ),
                        child: SvgPicture.asset(
                          AppAssets.svg.password,
                          width: 16.0,
                          color: AppColors.neutral2,
                        ),
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 16.0,
                      ),
                      filled: true,
                      fillColor: AppColors.neutral1,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        borderSide: BorderSide.none,
                      ),
                      counterText: '',
                    ),
                    onChanged: (value) {
                      setState(() {
                        _pass = value;
                      });
                    },
                    validator: (String? value) {
                      return value == null || value.isEmpty
                          ? S.of(context).inputErrorCheckPassword
                          : value.length < _minCharPass
                              ? S.of(context).inputErrorCheckPassword
                              : null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  SizedBox(
                      width: double.infinity,
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            FocusScope.of(context).unfocus();
                            if (_accountCorrect(_login, _pass)) {
                              FocusScope.of(context).unfocus();
                              formKey.currentState?.save();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const PersonsList()));
                            } else {
                              _showPassIncorrect(context);
                            }
                          }
                        },
                        child: Text(S.of(context).signIn),
                      )),
                  const SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text('${S.of(context).dontHaveAnAccountHint}?'),
                      ),
                      TextButton(
                        style: AppButtonStyles.text1,
                        child: Text(
                          S.of(context).create,
                        ),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ],
              ))),
    );
  }

  void _showPassIncorrect(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).wrongLoginOrPassword),
          actions: <Widget>[
            ElevatedButton(
              child: Text(S.of(context).close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
