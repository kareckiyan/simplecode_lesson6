import 'package:flutter/material.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_styles.dart';
import '../../generated/l10n.dart';
import 'package:intl/intl.dart';

import '../widgets/app_nav_bar.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SettingsScreenState();
}

class SettingsScreenState extends State {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const AppNavBar(current: 1),
      appBar: AppBar(
        title: Text(
          S.of(context).settings,
          style: AppStyles.s20w500,
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        foregroundColor: AppColors.mainText,
        elevation: 0.0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text("${S.of(context).language}: "),
              DropdownButton<String>(
                  value: Intl.getCurrentLocale(),
                  icon: const Icon(Icons.arrow_downward),
                  onChanged: (String? newLocale) {
                    setState(() {
                      S.load(Locale(newLocale!));
                    });
                  },
                  items: const [
                    DropdownMenuItem(
                      value: 'en',
                      child: Text("English"),
                    ),
                    DropdownMenuItem(
                      value: 'ru_RU',
                      child: Text("Русский"),
                    )
                  ]),
            ]),
          ],
        ),
      ),
    );
  }
}
