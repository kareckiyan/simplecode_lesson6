import 'package:flutter/material.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_styles.dart';
import '../../generated/l10n.dart';
import '../models/person.dart';
import '../settings/properties.dart';
import 'user_avatar.dart';

class MyGridView extends StatelessWidget {
  const MyGridView({
    Key? key,
    required this.personsList,
  }) : super(key: key);

  final List<Person> personsList;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      mainAxisSpacing: 20.0,
      crossAxisSpacing: 8.0,
      childAspectRatio: 0.8,
      crossAxisCount: 2,
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      children: personsList.map((person) {
        return InkWell(
          child: Column(
            children: [
              UserAvatar(
                person.image,
                radius: 60.0,
                margin: const EdgeInsets.only(bottom: 20.0),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            Properties.statusLabel(person.status).toUpperCase(),
                            style: AppStyles.s10w500.copyWith(
                              letterSpacing: 1.5,
                              color: Properties.statusColor(
                                person.status,
                              ),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            person.name ?? S.of(context).noData,
                            textAlign: TextAlign.center,
                            style: AppStyles.s16w500,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '${person.species ?? S.of(context).noData}, ${person.gender ?? S.of(context).noData}',
                            style: const TextStyle(
                              color: AppColors.neutral2,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        );
      }).toList(),
    );
  }
}
