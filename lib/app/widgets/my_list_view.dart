import 'package:flutter/material.dart';
import 'package:lesson5/app/settings/properties.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_styles.dart';
import '../../generated/l10n.dart';
import '../models/person.dart';
import 'user_avatar.dart';

class MyListView extends StatelessWidget {
  const MyListView({
    Key? key,
    required this.personsList,
  }) : super(key: key);

  final List<Person> personsList;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      itemCount: personsList.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: Row(
            children: [
              UserAvatar(
                personsList[index].image,
                margin: const EdgeInsets.only(right: 20.0),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            Properties.statusLabel(personsList[index].status)
                                .toUpperCase(),
                            style: AppStyles.s10w500.copyWith(
                              letterSpacing: 1.5,
                              color: Properties.statusColor(
                                personsList[index].status,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            personsList[index].name ?? S.of(context).noData,
                            style: AppStyles.s16w500.copyWith(
                              height: 1.6,
                              leadingDistribution: TextLeadingDistribution.even,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '${personsList[index].species ?? S.of(context).noData}, '
                            '${personsList[index].gender ?? S.of(context).noData}',
                            style: const TextStyle(
                              color: AppColors.neutral2,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
      separatorBuilder: (context, _) => const SizedBox(height: 26.0),
    );
  }
}
